package ecuaapp.com.todoadomicilio;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class RestaurantActivity extends AppCompatActivity {

    String[] nombre={"PIzza Place","Bola","MJ","Delice De France"};
    String[] descripcion={"Lorem Ipsum is simply dummy text of the printing and typesetting industry.","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","Lorem Ipsum is simply dummy text of the printing and typesetting industry.","Lorem Ipsum is simply dummy text of the printing and typesetting industry."};
    int[] images={R.drawable.restaurante1,R.drawable.restaurante2,R.drawable.restaurante3,R.drawable.restaurante4 };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        RecyclerView rv = (RecyclerView) findViewById(R.id.myRecycler);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setItemAnimator(new DefaultItemAnimator());

        RestaurantAdapter adapter = new RestaurantAdapter(this, nombre, descripcion, images);
        rv.setAdapter(adapter);

    }

    @Override
    public void onBackPressed(){
        Intent myIntent = new Intent(RestaurantActivity.this, MainActivity.class);
//                        myIntent.putExtra("key", value); //Optional parameters
        RestaurantActivity.this.startActivity(myIntent);
        // do something here and don't write super.onBackPressed()
    }

}
