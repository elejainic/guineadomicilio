package ecuaapp.com.todoadomicilio;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by jaime.esono on 20/09/2016.
 */
public class RestaurantAdapter extends RecyclerView.Adapter<MyHolder> {

    Context c;
    String[] enunciado;
    String[] descripcion;
    int[] imgs;

    public RestaurantAdapter(Context ctx, String[] names, String[] descripcion, int[] images){
        this.c=ctx;
        this.enunciado=names;
        this.descripcion=descripcion;
        this.imgs=images;
    }



    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.restaurant_model,null);

        MyHolder holder=new MyHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, final int position) {

        holder.img.setImageResource(imgs[position]);
        holder.nameTxt.setText(enunciado[position]);
        holder.posTxt.setText(descripcion[position]);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


                Intent intent = new Intent(c, PizzaPlaceActivity.class);
                intent.putExtra("botones",pos);
                c.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {

       return enunciado.length;
    }
}
