package ecuaapp.com.todoadomicilio;

import java.util.UUID;

/**
 * Created by jaime.esono on 28/09/2016.
 */
public class Product {
    private UUID mId;
    private String mTitle;
    private String mDescripcion;
    private boolean mChecked;
    private boolean mUnoChecked;
    private boolean mEquisChecked;
    private boolean mDosChecked;
    private String mPrecio;
    private String mCantidad;
    private String mPartido;
    private String mCategoria;



    public Product() {
        // Generate unique identifier
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescripcion() {
        return mDescripcion;
    }

    public void setDescripcion(String descripcion) {
        mDescripcion = descripcion;
    }

    public String getPrecio() {
        return mPrecio;
    }

    public void setPrecio(String precio) {
        mPrecio = precio;
    }

    public String getCantidad() {
        return mCantidad;
    }

    public void setCantidad(String cantidad) {
        mCantidad = cantidad;
    }

    public String getPartido() {
        return mPartido;
    }

    public void setPartido(String partido) {
        mPartido = partido;
    }

    public boolean isChecked() {
        return mChecked;
    }
    public void setChecked(boolean solved) {
        mChecked = solved;
    }

    public boolean unoisChecked() {
        return mUnoChecked;
    }
    public void setunoChecked(boolean resuelto) {
        mUnoChecked = resuelto;
    }

    public boolean equisisChecked() {
        return mEquisChecked;
    }
    public void setequisChecked(boolean done) {
        mEquisChecked = done;
    }

    public boolean dosisChecked() {
        return mDosChecked;
    }
    public void setdosChecked(boolean hecho) {
        mDosChecked = hecho;
    }

    public String getCategoria() {
        return mCategoria;
    }
    public void setCategoria(String categoria) {
        mCategoria = categoria;
    }

}
