package ecuaapp.com.todoadomicilio;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.List;
import java.util.UUID;

/**
 * Created by jaime.esono on 29/09/2016.
 */
public class ProductListFragment extends android.support.v4.app.Fragment{
    private static final String TAG = "ProductListFragment";
    private static final int REQUEST_CRIME = 1;

    private RecyclerView mProductRecyclerView;
    private ProductAdapter mAdapter;

    private boolean mItemhasChanged = false;
    private UUID mItemChangedId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_list_fragment, container, false);

        mProductRecyclerView = (RecyclerView) view
                .findViewById(R.id.product_recycler_view);
        mProductRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());
        mProductRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ProductActivity activity = (ProductActivity) getActivity();
        Integer myDataFromActivity = activity.getMyData();

        updateUI();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    private void updateUI() {

        ProductActivity activity = (ProductActivity) getActivity();
        Integer myDataFromActivity = activity.getMyData();

        ProductLab productLab = ProductLab.get(getActivity());
        GlobalClass globalVariable = (GlobalClass) getActivity().getApplicationContext();
        String name  = globalVariable.getName();
        List<Product> products = null;

        if(name == "MJ"){
            products = productLab.getProductsMJ(myDataFromActivity);
        }else if(name =="Pizza Place") {
            products = productLab.getProducts(myDataFromActivity);
        }else if(name =="Quiniela") {
            products = productLab.getProductsQuiniela(myDataFromActivity);
        }

        if (mAdapter == null) {
            mAdapter = new ProductAdapter(products);
            mProductRecyclerView.setAdapter(mAdapter);
        } else {
            if (mItemhasChanged) {
                int mItemChangedPosition = mAdapter.getCrimeIndex(mItemChangedId);
                Log.d(TAG, "Changed position :" + mItemChangedPosition);
                mAdapter.notifyItemChanged(mItemChangedPosition);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: ");
        if (resultCode != Activity.RESULT_OK) {
            mItemhasChanged = false;
            Log.d(TAG, "not OK: " + resultCode);
            return;
        }

//        if (requestCode == REQUEST_CRIME) {
//            if (data != null) {
//                Log.d(TAG, "data != null");
//                mItemhasChanged = CrimeActivity.hasCrimeChanged(data);
//                mItemChangedId = CrimeActivity.getCrimeId(data);
//            }
//        }
    }

    private class ProductHolder extends RecyclerView.ViewHolder {

        private Product mProduct;

        private CheckBox mCheckedBox;
        private TextView mTitle;
        private TextView mDescripcion;
        private TextView mPrecio;
        private EditText mCantidad;
        private TextView mPrecioTag;
        private TextView mMoneda;
        private TextView mCantidadTag;

        private TextView mPartido;
        private TextView mUno;
        private TextView mEquis;
        private TextView mDos;
        private CheckBox mCheckboxUno;
        private CheckBox mCheckboxEquis;
        private CheckBox mCheckboxDos;





        public ProductHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.product_title);
            mDescripcion = (TextView) itemView.findViewById(R.id.product_descripcion);
            mPrecio = (TextView) itemView.findViewById(R.id.txtPrecio);
            mCantidad = (EditText) itemView.findViewById(R.id.num_cantidad);
            mPrecioTag = (TextView) itemView.findViewById(R.id.txtPrecio_tag);
            mMoneda = (TextView) itemView.findViewById(R.id.moneda);

            mCantidadTag = (TextView) itemView.findViewById(R.id.txtCantidad_tag);

            mPartido = (TextView) itemView.findViewById(R.id.txtPartido);
            mUno = (TextView) itemView.findViewById(R.id.uno_tag);
            mEquis = (TextView) itemView.findViewById(R.id.equis_tag);
            mDos = (TextView) itemView.findViewById(R.id.dos_tag);





//            mCantidad.addTextChangedListener(new TextWatcher() {
//                public void afterTextChanged(Editable s) {}
//
//                public void beforeTextChanged(CharSequence s, int start,
//                                              int count, int after) {
//                }
//
//                public void onTextChanged(CharSequence s, int start,
//                                          int before, int count) {
//                    if(mCantidad.getTag()!=null){
//                        Log.d(TAG, "fromLocation text : "
//                                + mCantidad.getText().toString());
//                    }
//                    // save ans to sharedpreferences or Database
//                }
//            });

            mCheckedBox = (CheckBox)
                    itemView.findViewById(R.id.product_selected);
            mCheckboxUno = (CheckBox)
                    itemView.findViewById(R.id.uno);
            mCheckboxEquis = (CheckBox)
                    itemView.findViewById(R.id.equis);
            mCheckboxDos = (CheckBox)
                    itemView.findViewById(R.id.dos);

        }

        public void bindProduct(Product product) {
            mProduct = product;
            mTitle.setText(mProduct.getTitle());
            mDescripcion.setText(mProduct.getDescripcion());
            mPrecio.setText(mProduct.getPrecio());
            mPartido.setText(mProduct.getPartido());
            // Hide elements
            GlobalClass globalVariable = (GlobalClass) getActivity().getApplicationContext();
            String name  = globalVariable.getName();
            if(name =="Quiniela") {

                mProduct.setCategoria("Quiniela");

                mTitle.setVisibility(View.GONE);
                mDescripcion.setVisibility(View.GONE);
                mPrecio.setVisibility(View.GONE);
                mCantidad.setVisibility(View.GONE);
                mCheckedBox.setVisibility(View.GONE);
                mMoneda.setVisibility(View.GONE);
                mPrecioTag.setVisibility(View.GONE);
                mCantidadTag.setVisibility(View.GONE);

            }else {
                mUno.setVisibility(View.GONE);
                mEquis.setVisibility(View.GONE);
                mDos.setVisibility(View.GONE);
                mCheckboxUno.setVisibility(View.GONE);
                mCheckboxEquis.setVisibility(View.GONE);
                mCheckboxDos.setVisibility(View.GONE);

            }




//            mProduct.setCantidad(mCantidad.getText().toString());
            mCantidad.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

//                    Toast.makeText(getActivity(), mCantidad.getText().toString(),
//                            Toast.LENGTH_SHORT).show();
                    mProduct.setCantidad(mCantidad.getText().toString());
                    // save ans to sharedpreferences or Database
                }
            });
            mCheckedBox.setChecked(mProduct.isChecked());
            mCheckedBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // Set the crime's solved property
                    mProduct.setChecked(isChecked);
                }
            });
            mCheckboxUno.setChecked(mProduct.unoisChecked());
            mCheckboxUno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean unoisChecked) {
                    // Set the crime's solved property
                    mProduct.setunoChecked(unoisChecked);
                }
            });
            mCheckboxEquis.setChecked(mProduct.equisisChecked());
            mCheckboxEquis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean equisisChecked) {
                    // Set the crime's solved property
                    mProduct.setequisChecked(equisisChecked);
                }
            });
            mCheckboxDos.setChecked(mProduct.dosisChecked());
            mCheckboxDos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean dosisChecked) {
                    // Set the crime's solved property
                    mProduct.setdosChecked(dosisChecked);
                }
            });

        }


    }

    private class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {

        private List<Product> mProduct;

        public ProductAdapter(List<Product> products) {
            mProduct = products;
        }



        @Override
        public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.product_model_fragment, null);

            ProductHolder holder=new ProductHolder(v);

            return holder;
        }

        @Override
        public void onBindViewHolder(ProductHolder holder, int position) {
            Product product = mProduct.get(position);



            holder.bindProduct(product);
            holder.mProduct.setChecked(product.isChecked());
//            holder.mProduct.setCantidad(product.setCantidad(product));
            holder.mCantidad.setText(product.getCantidad());

//            holder.mCantidad.setTag(position);
//            holder.mCantidad.setText(product);

        }

        @Override
        public int getItemCount() {
            return mProduct.size();
        }

        private int getCrimeIndex(UUID productId) {
            for (int i = 0; i < mProduct.size(); i++) {
                Product product = mProduct.get(i);
                if (product.getId().equals(productId)) {
                    return i;
                }
            }
            return -1;
        }


    }
}
