package ecuaapp.com.todoadomicilio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gv;
    String[] names = {"Restaurante", "Farmacia", "loterias", "compra", "gas"};
    int[] images={R.drawable.restauranticon,R.drawable.farmacia1,R.drawable.apuestas,R.drawable.cart1,R.drawable.propane};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gv=(GridView) findViewById(R.id.gridView1);
        Adapter adapter =new Adapter(this, getIconos());
        gv.setAdapter(adapter);
        gv.setVerticalScrollBarEnabled(false);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    int pos=view.getId();

                switch(position){

                    case 0: /** Start a new Activity MyCards.java */
                        Intent myIntent = new Intent(MainActivity.this, RestaurantActivity.class);
//                        myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent);
                        break;
                    case 2: /** Start a new Activity MyCards.java */
                        Intent myIntent1 = new Intent(MainActivity.this, ActivityLoterias.class);
//                        myIntent.putExtra("key", value); //Optional parameters
                        MainActivity.this.startActivity(myIntent1);
                        break;
                }


            }
        });
    }

    private ArrayList<icono> getIconos()
    {
        ArrayList<icono> iconos=new ArrayList<>();
        iconos.add(new icono(names[0], images[0]));
        iconos.add(new icono(names[1], images[1]));
        iconos.add(new icono(names[2], images[2]));
        iconos.add(new icono(names[3], images[3]));
        iconos.add(new icono(names[4], images[4]));

        return iconos;

    }
}
