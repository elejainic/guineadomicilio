package ecuaapp.com.todoadomicilio;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class ReciboActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1234;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.recibo_list);

        if (fragment == null) {
            fragment = new ReciboListFragment();
            fm.beginTransaction()
                    .add(R.id.recibo_list, fragment)
                    .commit();

//            ActionBar ab = getActionBar();
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Total de pedido es: " + getTotal());//            ab.setSubtitle("sub-title");

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
//            FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);

            if (fab != null) {
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int choice) {
                                switch (choice) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        ReciboLab reciboLab = ReciboLab.get(ReciboActivity.this);
                                        List<ReciboModel> recibos = reciboLab.getRecibo();
                                        recibos.clear();
                                        finish();
                                        startActivity(getIntent());

                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:
                                        break;
                                }
                            }
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(ReciboActivity.this);
                        builder.setMessage("Quieres borrar el pedido?")
                                .setPositiveButton("Si", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();
                        //                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        //                            .setAction("Action", null).show();
                    }
                });
            }

            assert fab1 != null;
            assert fab1 != null;
            fab1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final Dialog dialog = new Dialog(ReciboActivity.this);
                    dialog.setContentView(R.layout.costumoption);
                    dialog.setTitle("Opciones para hacer pedido");

                    // set the custom dialog components - text, image and button
//                    TextView text = (TextView) dialog.findViewById(R.id.text);
//                    text.setText("Android custom dialog example!");
//                    ImageView image = (ImageView) dialog.findViewById(R.id.image);
//                    image.setImageResource(R.drawable.ic_launcher);

                    ImageView dialogButton = (ImageView) dialog.findViewById(R.id.image2);
                    ImageView dialogButton1 = (ImageView) dialog.findViewById(R.id.image3);
                    ImageView dialogButton2 = (ImageView) dialog.findViewById(R.id.image4);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PackageManager pm = getPackageManager();
                            try {

                                Intent waIntent = new Intent(Intent.ACTION_SEND);
                                waIntent.setType("text/plain");

                                ReciboLab reciboLab = ReciboLab.get(ReciboActivity.this);
                                List<ReciboModel> recibos = reciboLab.getRecibo();
                                StringBuilder sb = new StringBuilder();
                                int size = recibos.size();
                                boolean appendSeparator = false;
                                GlobalClass globalVariable = (GlobalClass) getApplicationContext();
                                String jornada = globalVariable.getJornada();
                                for (int y = 0; y < size; y++) {
                                    if(y == 0 && (recibos.get(y).getCategoria() == "quiniela" ||
                                            recibos.get(y).getCategoria() == "Jornada 15" ||
                                            recibos.get(y).getCategoria() == "Jornada 16")){
                                        sb.append("Quiniela ");
                                        sb.append("\n ");
                                        sb.append("--------------");
                                        sb.append("\n ");

                                    }

                                    if(y == 0 && recibos.get(y).getCategoria() == "Jornada 15"){
                                        sb.append("Jornada 15 ");
                                        sb.append("\n ");
                                    }else if (y == 0 && recibos.get(y).getCategoria() == "Jornada 15"){
                                        sb.append("Jornada 16 ");
                                        sb.append("\n ");
                                    }


//

                                    if (recibos.get(y).getCategoria() == "quiniela" ||
                                            recibos.get(y).getCategoria() == "Jornada 15" ||
                                            recibos.get(y).getCategoria() == "Jornada 16") {
                                        if (appendSeparator)
//                                            sb.append(','); // a comma
                                        appendSeparator = true;

                                        sb.append(recibos.get(y).getPartido());
                                        sb.append(" ");
                                        if(recibos.get(y).getResultadoUno()!= null) {
                                            sb.append(recibos.get(y).getResultadoUno());
                                        } else{
                                            sb.append(" ");
                                        }
                                        sb.append(" ");
                                        if(recibos.get(y).getResultadoEquis()!= null) {
                                            sb.append(recibos.get(y).getResultadoEquis());
                                        } else{
                                            sb.append(" ");
                                        }
                                        sb.append(" ");
                                        if(recibos.get(y).getmResultadoDos()!= null) {
                                            sb.append(recibos.get(y).getmResultadoDos());
                                        } else{
                                            sb.append(" ");
                                        }
                                        sb.append("\n ");
                                        if(y == 14){
                                            sb.append("--------------");
                                            sb.append("\n ");
                                        }
                                    } else {
                                        if (appendSeparator)
                                            sb.append(','); // a comma
                                        appendSeparator = true;

                                        sb.append(recibos.get(y).getTitle());
                                        sb.append(" ");
                                        sb.append(recibos.get(y).getPrecio());
                                        sb.append(" ");
                                        sb.append("x");
                                        sb.append(recibos.get(y).getCantidad());

                                        sb.append("\n ");
                                    }
                                }
                                sb.append("El total es: ");
                                sb.append(getTotal());
//                                sb.append("\n ");
//                                sb.append("--------------------");
//                                sb.append("Quiniela");
                                String text = sb.toString();


                                PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                                //Check if package exists or not. If not then code
                                //in catch block will be called
                                waIntent.setPackage("com.whatsapp");

                                waIntent.putExtra(Intent.EXTRA_TEXT, text);
                                startActivity(Intent.createChooser(waIntent, "Share with"));

                            } catch (PackageManager.NameNotFoundException e) {
                                Toast.makeText(ReciboActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }
                    });

                    dialogButton1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            String phoneNumber = "222716427";
                            String smsBody = "This is an SMS!";

                            ReciboLab reciboLab = ReciboLab.get(ReciboActivity.this);
                            List<ReciboModel> recibos = reciboLab.getRecibo();
                            StringBuilder sb = new StringBuilder();
                            int size = recibos.size();
                            boolean appendSeparator = false;
                            for (int y = 0; y < size; y++) {

                                if(y == 0 && (recibos.get(y).getCategoria() == "quiniela" ||
                                        recibos.get(y).getCategoria() == "Jornada 15" ||
                                        recibos.get(y).getCategoria() == "Jornada 16")){
                                    sb.append("Quiniela ");
                                    sb.append("\n ");
                                    sb.append("--------------");
                                    sb.append("\n ");

                                }

                                if(y == 0 && recibos.get(y).getCategoria() == "Jornada 15"){
                                    sb.append("Jornada 15 ");
                                    sb.append("\n ");
                                }else if (y == 0 && recibos.get(y).getCategoria() == "Jornada 15"){
                                    sb.append("Jornada 16 ");
                                    sb.append("\n ");
                                }


//

                                if (recibos.get(y).getCategoria() == "quiniela" ||
                                        recibos.get(y).getCategoria() == "Jornada 15" ||
                                        recibos.get(y).getCategoria() == "Jornada 16") {
                                    if (appendSeparator)
//                                            sb.append(','); // a comma
                                        appendSeparator = true;

                                    sb.append(recibos.get(y).getPartido());
                                    sb.append(" ");
                                    if(recibos.get(y).getResultadoUno()!= null) {
                                        sb.append(recibos.get(y).getResultadoUno());
                                    } else{
                                        sb.append(" ");
                                    }
                                    sb.append(" ");
                                    if(recibos.get(y).getResultadoEquis()!= null) {
                                        sb.append(recibos.get(y).getResultadoEquis());
                                    } else{
                                        sb.append(" ");
                                    }
                                    sb.append(" ");
                                    if(recibos.get(y).getmResultadoDos()!= null) {
                                        sb.append(recibos.get(y).getmResultadoDos());
                                    } else{
                                        sb.append(" ");
                                    }
                                    sb.append("\n ");
                                    if(y == 14){
                                        sb.append("--------------");
                                        sb.append("\n ");
                                    }
                                } else {
                                    if (appendSeparator)
                                        sb.append(','); // a comma
                                    appendSeparator = true;

                                    sb.append(recibos.get(y).getTitle());
                                    sb.append(" ");
                                    sb.append(recibos.get(y).getPrecio());
                                    sb.append(" ");
                                    sb.append("x");
                                    sb.append(recibos.get(y).getCantidad());

                                    sb.append("\n ");
                                }
                            }
                            sb.append("El total es: ");
                            sb.append(getTotal());
                            String text = sb.toString();

                            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                            smsIntent.setType("vnd.android-dir/mms-sms");
                            smsIntent.putExtra("address", phoneNumber);
                            smsIntent.putExtra("sms_body", text);
                            startActivity(smsIntent);

                        }
                    });
                    dialogButton2.setOnClickListener(new View.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {

                            if (ContextCompat.checkSelfPermission(ReciboActivity.this,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions(ReciboActivity.this,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        MY_PERMISSIONS_REQUEST_CALL_PHONE);
                            } else {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:+240222716427"));

                                startActivity(callIntent);
                            }
                        }
                    });

                    dialog.show();

//                    dialogButton1.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });

//                    dialog.show();

//                    String phoneNumber = "222716427";
//                    String smsBody = "This is an SMS!";
//
//                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
//// Invokes only SMS/MMS clients
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//// Specify the Phone Number
//                    smsIntent.putExtra("address", phoneNumber);
//// Specify the Message
//                    smsIntent.putExtra("sms_body", smsBody);
//
//// Shoot!
//                    startActivity(smsIntent);

//                    SmsManager smsManager = SmsManager.getDefault();
//                    PendingIntent sentPI;
//                    String SENT = "SMS_SENT";
//                    sentPI = PendingIntent.getBroadcast(ReciboActivity.this, 0,new Intent(SENT), 0);
//                    smsManager.sendTextMessage("222716427", null, "Hello there!", sentPI, null);




//                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                            .setAction("Action", null).show();
                }
            });


        }
        }





    private int getTotal(){

        ReciboLab reciboLab = ReciboLab.get(this);
        List<ReciboModel> recibos = reciboLab.getRecibo();
        int suma= 0;
        int entero;
        for (int i = 0; i < recibos.size(); i++){
            entero = Integer.valueOf(recibos.get(i).getTotal());
            suma = suma + entero;
        }
       return suma;
    }

}
