package ecuaapp.com.todoadomicilio;

/**
 * Created by jaime.esono on 28/11/2016.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class ActivityLoterias extends AppCompatActivity {
    Context ctx;
    GridView gv;
//    String[] names = {"Quiniela", "Bonoloto", "Primitiva", "Euromillones", "El gordo", "Quinigol"};
    int[] images={R.drawable.quiniela,R.drawable.bonoloto,R.drawable.primitiva,R.drawable.euromillones,R.drawable.elgordodelaprimitiva,R.drawable.quinigol};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loterias);

        gv=(GridView) findViewById(R.id.gridView2);
        AdapterLoteria adapter =new AdapterLoteria(this, getIconos());
        gv.setAdapter(adapter);
        gv.setVerticalScrollBarEnabled(false);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                int pos=view.getId();

                switch(position){

                    case 0: /** Start a new Activity MyCards.java */
                        GlobalClass globalVariable = (GlobalClass) getApplicationContext();
                        globalVariable.setName("Quiniela");

                        Intent intent = new Intent(ActivityLoterias.this, QuinielaFechasActivy.class);
                        intent.putExtra("presionado", position);
                        ActivityLoterias.this.startActivity(intent);
                        break;
                }


            }
        });
    }

    private ArrayList<IconoApuestas> getIconos()
    {
        ArrayList<IconoApuestas> iconos=new ArrayList<>();
        iconos.add(new IconoApuestas(images[0]));
        iconos.add(new IconoApuestas(images[1]));
        iconos.add(new IconoApuestas(images[2]));
        iconos.add(new IconoApuestas(images[3]));
        iconos.add(new IconoApuestas(images[4]));
        iconos.add(new IconoApuestas(images[5]));


        return iconos;

    }

    @Override
    public void onBackPressed(){
        Intent myIntent = new Intent(ActivityLoterias.this, MainActivity.class);
//                        myIntent.putExtra("key", value); //Optional parameters
        ActivityLoterias.this.startActivity(myIntent);
        // do something here and don't write super.onBackPressed()
    }
}

