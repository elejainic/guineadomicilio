package ecuaapp.com.todoadomicilio;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class PizzaPlaceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_place);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new PizzaPlaceFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int caller = getIntent().getIntExtra("botones", 0);
                    String llamada = String.valueOf(caller);
                    Snackbar.make(view, llamada, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    }

    @Override
    public void onBackPressed(){
        Intent myIntent = new Intent(PizzaPlaceActivity.this, RestaurantActivity.class);
//                        myIntent.putExtra("key", value); //Optional parameters
        PizzaPlaceActivity.this.startActivity(myIntent);
        // do something here and don't write super.onBackPressed()
    }

    public Integer getMyData() {
        int caller = getIntent().getIntExtra("botones", 0);
        if (caller == 0){
            GlobalClass globalVariable = (GlobalClass) getApplicationContext();
            globalVariable.setName("MJ");

            String name  = globalVariable.getName();
        }else if(caller == 1){
            GlobalClass globalVariable = (GlobalClass) getApplicationContext();
            globalVariable.setName("Pizza Place");
        }
        return caller;
    }
}
