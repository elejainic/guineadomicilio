package ecuaapp.com.todoadomicilio;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jaime.esono on 28/09/2016.
 */
public class ProductLab {
    private static ProductLab sProductLab;
    //    String[] nombreArrozRes1 = {"Arroz con sepia y gambas", "Arroz con champiñones y espárragos trigueros", "Paella de pollo y judías", " maki-sushi-de-aguacate-y-surimi\n" +
//            "\n" +
//            "Maki sushi de aguacate y surimi"};
//    String[] precioArrozRest1 = {"1000", "2000", "6000", " 3000" +
//            "\n" +
//            "3500"};
//    String[] descripArrozRes1 = {"descripcion1", "descripcion2", "descripcion3", "descripcion4" +
//            "\n" +
//            "descripcion5"};
    private List<Product> mArrozRes1;
    private List<Product> mArrozResMJ;
    private List<Product> mCarneRes1;
    private List<Product> mEnsaladaRes1;
    private List<Product> mProduct;
    private List<Product> mJornada31;
    private List<Product> mJornada32;

    public static ProductLab get(Context context) {
        if (sProductLab == null) {
            sProductLab = new ProductLab(context);
        }
        return sProductLab;
    }

    private ProductLab(Context context) {
        mArrozRes1 = new ArrayList<>();
        Product product1 = new Product();
        product1.setTitle("Arroz con sepia y gambas");
        product1.setDescripcion("descripcion1");
        product1.setPrecio("5000");
        mArrozRes1.add(product1);
        Product product2 = new Product();
        product2.setTitle("Arroz con champiñones y espárragos trigueros");
        product2.setDescripcion("descripcion1");
        product2.setPrecio("3000");
        mArrozRes1.add(product2);
        Product product3 = new Product();
        product3.setTitle("Arroz a la milanesa");
        product3.setDescripcion("descripcion1");
        product3.setPrecio("4000");
        mArrozRes1.add(product3);
        Product product4 = new Product();
        product4.setTitle("Maki sushi de aguacate y surimi");
        product4.setDescripcion("descripcion1");
        product4.setPrecio("2400");
        mArrozRes1.add(product4);
        Product product5 = new Product();
        product5.setTitle("Arroz con patatas");
        product5.setDescripcion("descripcion1");
        product5.setPrecio("1000");
        mArrozRes1.add(product5);

        mArrozResMJ = new ArrayList<>();
        Product product16 = new Product();
        product16.setTitle("Arroz MJ");
        product16.setDescripcion("descripcion1");
        product16.setPrecio("5000");
        mArrozResMJ.add(product16);
        Product product17 = new Product();
        product17.setTitle("Paella MJ");
        product17.setDescripcion("descripcion1");
        product17.setPrecio("3000");
        mArrozResMJ.add(product17);

        mCarneRes1 = new ArrayList<>();
        Product product6 = new Product();
        product6.setTitle("Sobrebarriga al Horno");
        product6.setDescripcion("descripcion1");
        product6.setPrecio("5000");
        mCarneRes1.add(product6);
        Product product7 = new Product();
        product7.setTitle("Lasaña de plátano y carne");
        product7.setDescripcion("descripcion1");
        product7.setPrecio("3000");
        mCarneRes1.add(product7);
        Product product8 = new Product();
        product8.setTitle("Cerdo agridulce");
        product8.setDescripcion("descripcion1");
        product8.setPrecio("4000");
        mCarneRes1.add(product8);
        Product product9 = new Product();
        product9.setTitle("Costillas de cerdo");
        product9.setDescripcion("descripcion1");
        product9.setPrecio("2400");
        mCarneRes1.add(product9);
        Product product10 = new Product();
        product10.setTitle("Ahuletas de Ternera a la Mostaza");
        product10.setDescripcion("descripcion1");
        product10.setPrecio("1000");
        mCarneRes1.add(product10);

        mEnsaladaRes1 = new ArrayList<>();
        Product product11 = new Product();
        product11.setTitle("Ensalada de pepinillo");
        product11.setDescripcion("descripcion1");
        product11.setPrecio("2000");
        mEnsaladaRes1.add(product11);
        Product product12 = new Product();
        product12.setTitle("Ensalada de Espinacas");
        product12.setDescripcion("descripcion1");
        product12.setPrecio("3000");
        mEnsaladaRes1.add(product12);
        Product product13 = new Product();
        product13.setTitle("Ensalada de Frutas");
        product13.setDescripcion("descripcion1");
        product13.setPrecio("1700");
        mEnsaladaRes1.add(product13);
        Product product14 = new Product();
        product14.setTitle("Ensalada de Col");
        product14.setDescripcion("descripcion1");
        product14.setPrecio("3100");
        mEnsaladaRes1.add(product14);
        Product product15 = new Product();
        product15.setTitle("Ensalada de Cuscús");
        product15.setDescripcion("descripcion1");
        product15.setPrecio("1000");
        mEnsaladaRes1.add(product15);

        mJornada31 = new ArrayList<>();
        Product partido31 = new Product();
        partido31.setPartido("1. R.SOCIEDAD - VALENCIA");
        mJornada31.add(partido31);
        Product partido32 = new Product();
        partido32.setPartido("2. LAS PALMAS - LEGANÉS");
        mJornada31.add(partido32);
        Product partido33 = new Product();
        partido33.setPartido("3. R.MADRID - DEPORTIVO");
        mJornada31.add(partido33);
        Product partido34 = new Product();
        partido34.setPartido("4. EIBAR - ALAVÉS");
        mJornada31.add(partido34);
        Product partido35 = new Product();
        partido35.setPartido("5. CELTA - SEVILLA");
        mJornada31.add(partido35);
        Product partido36 = new Product();
        partido36.setPartido("6. ESPANYOL - SPORTING");
        mJornada31.add(partido36);
        Product partido37 = new Product();
        partido37.setPartido("7. BETIS - ATH.CLUB");
        mJornada31.add(partido37);
        Product partido38 = new Product();
        partido38.setPartido("8. GIMNÁSTIC - RAYO");
        mJornada31.add(partido38);
        Product partido39 = new Product();
        partido39.setPartido("9. ELCHE - GETAFE");
        mJornada31.add(partido39);
        Product partido40 = new Product();
        partido40.setPartido("10. LEVANTE - TENERIFE");
        mJornada31.add(partido40);
        Product partido41 = new Product();
        partido41.setPartido("11. R.SOCIEDAD - VALENCIA");
        mJornada31.add(partido41);
        Product partido42 = new Product();
        partido42.setPartido("12. CÓRDOBA - CÁDIZ");
        mJornada31.add(partido42);
        Product partido43 = new Product();
        partido43.setPartido("13. VALLADOLID - MIRANDÉS");
        mJornada31.add(partido43);
        Product partido44 = new Product();
        partido44.setPartido("14. LUGO - NUMANCIA");
        mJornada31.add(partido44);
        Product partido45 = new Product();
        partido45.setPartido("15. Villarreal  - At. Madrid");
        mJornada31.add(partido45);

        mJornada32 = new ArrayList<>();
        Product partido16 = new Product();
        partido16.setPartido("1. Dinamo Kiev - Besiktas");
        mJornada32.add(partido16);
        Product partido17 = new Product();
        partido17.setPartido("2. Basel - Arsenal");
        mJornada32.add(partido17);
        Product partido18 = new Product();
        partido18.setPartido("3. PSV - Rostov");
        mJornada32.add(partido18);
        Product partido19 = new Product();
        partido19.setPartido("4. PSG - Ludogorets");
        mJornada32.add(partido19);
        Product partido20 = new Product();
        partido20.setPartido("5. Barcelona - Möncheng");
        mJornada32.add(partido20);
        Product partido21 = new Product();
        partido21.setPartido("6. Man. City - Celtic");
        mJornada32.add(partido21);
        Product partido22 = new Product();
        partido22.setPartido("7. Benfica - Napoles");
        mJornada32.add(partido22);
        Product partido23 = new Product();
        partido23.setPartido("8. Oporto - Leicester");
        mJornada32.add(partido23);
        Product partido24 = new Product();
        partido24.setPartido("9. Leverkusen - Mónaco");
        mJornada32.add(partido24);
        Product partido25 = new Product();
        partido25.setPartido("10. R. Madrid - Dortmund");
        mJornada32.add(partido25);
        Product partido26 = new Product();
        partido26.setPartido("11. Legia Varsovia - Sporting");
        mJornada32.add(partido26);
        Product partido27 = new Product();
        partido27.setPartido("12. Juventus - Dinamo Zagreb");
        mJornada32.add(partido27);
        Product partido28 = new Product();
        partido28.setPartido("13. Tottenham - Cska Moskva");
        mJornada32.add(partido28);
        Product partido29 = new Product();
        partido29.setPartido("14. Lyon - Sevilla");
        mJornada32.add(partido29);
        Product partido30 = new Product();
        partido30.setPartido("15. Bayern - At. Madrid");
        mJornada32.add(partido30);



        mProduct = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Product product = new Product();
            product.setTitle("Producto #" + i);
            product.setDescripcion("descripcion" + i); // Every other one
            product.setPrecio("5000" + i);
            mProduct.add(product);
        }
    }

    public List<Product> getProducts(int i) {
        if (i == 0) {
            return mArrozRes1;

        } else if (i == 1) {
            return mCarneRes1;
        }else if (i == 2) {
            return mEnsaladaRes1;
        }else{
            return mProduct;
        }
    }

    public List<Product> getProductsMJ(int i) {
        if (i == 0) {
            return mArrozResMJ;

        } else if (i == 1) {
            return mCarneRes1;
        }else if (i == 2) {
            return mEnsaladaRes1;
        }else{
            return mProduct;
        }
    }

    public List<Product> getProductsQuiniela(int i) {
        if (i == 0) {
            return mJornada31;

        } else {
            return mJornada32;
        }

    }



    public Product getProduct(UUID id) {
        for (Product product : mProduct) {
            if (product.getId().equals(id)) {
                return product;
            }
        }
        return null;
    }
}
