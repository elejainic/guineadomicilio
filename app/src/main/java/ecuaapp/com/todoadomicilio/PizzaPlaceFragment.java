package ecuaapp.com.todoadomicilio;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by jaime.esono on 24/09/2016.
 */
public class PizzaPlaceFragment extends Fragment {

    private RecyclerView rv;

    String[] nombre={"Arroces","Carnes","Ensaladas","Hamburguesas","Licores","Pescados","Postres","Vinos"};
    int[] images={R.drawable.arroz,R.drawable.carnes,R.drawable.ensaladas,R.drawable.hamburguesa,R.drawable.licor,R.drawable.pescado,R.drawable.postres,R.drawable.vinos};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tipo_comida_fragment, container, false);
        PizzaPlaceActivity activity = (PizzaPlaceActivity) getActivity();
        Integer myDataFromActivity = activity.getMyData();

        rv = (RecyclerView) view
                .findViewById(R.id.crime_recycler_view);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));

//        RecyclerView rv = (RecyclerView) view.findViewById(R.id.myRecycler);
//
//        rv.setLayoutManager(new LinearLayoutManager(this));
//        rv.setItemAnimator(new DefaultItemAnimator());

        TipoComidaAdapter adapter = new TipoComidaAdapter(getContext(), nombre, images, myDataFromActivity);
        rv.setAdapter(adapter);

        return view;
    }


}
