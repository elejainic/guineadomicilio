package ecuaapp.com.todoadomicilio;

/**
 * Created by jaime.esono on 13/09/2016.
 */
public class icono {

    public icono(String name, int img) {

        this.img=img;
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    private String name;
    private int img;


}
