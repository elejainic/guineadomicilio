package ecuaapp.com.todoadomicilio;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jaime.esono on 25/09/2016.
 */
public class TipoComidaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView img;
    TextView nameTxt;
    ItemClickListener itemClickListener;

    public TipoComidaHolder(View itemView){
        super(itemView);

        img= (ImageView) itemView.findViewById(R.id.icon_tipo_comida);
        nameTxt = (TextView) itemView.findViewById(R.id.nombre_tipo_comida);


        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        this.itemClickListener.onItemClick(v,getLayoutPosition());

    }

    public void setItemClickListener(ItemClickListener ic)
    {
        this.itemClickListener=ic;
    }
}

