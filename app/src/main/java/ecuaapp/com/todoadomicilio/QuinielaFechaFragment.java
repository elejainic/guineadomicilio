package ecuaapp.com.todoadomicilio;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

/**
 * Created by jaime.esono on 03/12/2016.
 */
public class QuinielaFechaFragment extends Fragment {
    private RecyclerView rv;

    String[] nombre={"Jornada 15","Jornada 16","Jornada 17","Jornada 18","Jornada 19","Jornada 20",
            "Jornada 21","Jornada 22", "Jornada 23","Jornada 24","Jornada 25","Jornada 26","Jornada 27",
            "Jornada 28","Jornada 29", "Jornada 30", "Jornada 31", "Jornada 32", "Jornada 33", "Jornada 34",
            "Jornada 35", "Jornada 36","Jornada 37","Jornada 38","Jornada 39"};
//    int[] images={R.drawable.arroz,R.drawable.carnes,R.drawable.ensaladas,R.drawable.hamburguesa,R.drawable.licor,R.drawable.pescado,R.drawable.postres,R.drawable.vinos};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.quinielas_fecha_fragment, container, false);
        QuinielaFechasActivy activity = (QuinielaFechasActivy) getActivity();
//        Integer myDataFromActivity = activity.getMyData();

        rv = (RecyclerView) view
                .findViewById(R.id.quiniela_recycler_view);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());

//        RecyclerView rv = (RecyclerView) view.findViewById(R.id.myRecycler);
//
//        rv.setLayoutManager(new LinearLayoutManager(this));
//        rv.setItemAnimator(new DefaultItemAnimator());

        QuinielaFechaAdapter adapter = new QuinielaFechaAdapter(getContext(), nombre);
        rv.setAdapter(adapter);

        return view;
    }

}
