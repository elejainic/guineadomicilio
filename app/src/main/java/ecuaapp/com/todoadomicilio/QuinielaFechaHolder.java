package ecuaapp.com.todoadomicilio;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jaime.esono on 03/12/2016.
 */
public class QuinielaFechaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    TextView nameTxt;
    ItemClickListener itemClickListener;

    public QuinielaFechaHolder(View itemView){
        super(itemView);


        nameTxt = (TextView) itemView.findViewById(R.id.nombre_quiniela_fecha);


        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        this.itemClickListener.onItemClick(v,getLayoutPosition());

    }

    public void setItemClickListener(ItemClickListener ic)
    {
        this.itemClickListener=ic;
    }
}

