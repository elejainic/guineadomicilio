package ecuaapp.com.todoadomicilio;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jaime.esono on 03/10/2016.
 */
public class ReciboLab {
    private static ReciboLab sReciboLab;

    private List<ReciboModel> mRecibo;

    public static ReciboLab get(Context context) {
        if (sReciboLab == null) {
            sReciboLab = new ReciboLab(context);
        }
        return sReciboLab;
    }

    private ReciboLab(Context context) {
        mRecibo = new ArrayList<>();

//        for (int i = 0; i < 100; i++) {
//            ReciboModel recibo = new ReciboModel();
//            recibo.setTitle("Producto #" + i);
//            recibo.setDescripcion("descripcion" + i); // Every other one
//            recibo.setPrecio("5000" + i);
//            recibo.setCantidad("2" + i);
//            mRecibo.add(recibo);
//        }

    }

    public List<ReciboModel> getRecibo() {
        return mRecibo;
    }

    public ReciboModel getRecibo(UUID id) {
        for (ReciboModel recibo : mRecibo) {
            if (recibo.getId().equals(id)) {
                return recibo;
            }
        }
        return null;
    }
}
