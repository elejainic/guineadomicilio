package ecuaapp.com.todoadomicilio;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity  extends FragmentActivity {


    Context c;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.product_list);

        if (fragment == null) {
            fragment = new ProductListFragment();
            fm.beginTransaction()
                    .add(R.id.product_list, fragment)
                    .commit();

//            Intent intent = getIntent();
////             String arroces = intent.getStringExtra("arroces");
//            int caller = getIntent().getIntExtra("button", 0);
//            if (caller == 0){
//                GlobalClass globalVariable = (GlobalClass) getApplicationContext();
//                globalVariable.setJornada("Jornada15");
//                Toast.makeText(getApplicationContext(), "Jornada 15", Toast.LENGTH_SHORT).show();
//            }

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    int llamadar = getIntent().getIntExtra("button", 0);
//                    String llamada = String.valueOf(llamadar);


                        GlobalClass globalVariable = (GlobalClass) getApplicationContext();
//                        globalVariable.setName("Name here");
//                        String name  = globalVariable.getName();
//
////
//                    Snackbar.make(view, name, Snackbar.LENGTH_LONG)
//                            .setAction("Action", null).show();


                    updateUI();
//
//                    String resultado = getMyData().toString();
//
//                    Snackbar.make(view, resultado, Snackbar.LENGTH_SHORT).show();

//                    int caller = getIntent().getIntExtra("button", 0);
//                    switch (caller) {
//                        case 0:
//                            Snackbar.make(view, "carnes", Snackbar.LENGTH_SHORT).show();
//                            //do something
//                            break;
//                        case 1:
//                            Snackbar.make(view, "pescado", Snackbar.LENGTH_SHORT).show();
//                            //do something
//                            break;
//                        case R.id.imageButton3:
//                            //do something
//                            break;
//                        case R.id.imageButton4:
//                            //do something
//                            break;
//                        case R.id.imageButton5:
//                            //do something
//                            break;
//                        case R.id.imageButton6:
//                            //do something
//                            break;
//                    }




//                    Intent myIntent = new Intent(ProductActivity.this, ReciboActivity.class);
////                        myIntent.putExtra("key", value); //Optional parameters
//                    ProductActivity.this.startActivity(myIntent);
                    }
            });
        }



    }

    public Integer getMyData() {
        int caller = getIntent().getIntExtra("button", 0);
        return caller;
    }
    @Override
    public void onBackPressed() {

        GlobalClass globalVariable = (GlobalClass) getApplicationContext();
        String name = globalVariable.getName();

        if (name == "MJ") {
            Intent intent = new Intent(ProductActivity.this, PizzaPlaceActivity.class);
            intent.putExtra("botones",0);
            ProductActivity.this.startActivity(intent);
            // do something here and don't write super.onBackPressed()

        } else if (name == "Pizza Place") {

            Intent intent = new Intent(ProductActivity.this, PizzaPlaceActivity.class);
            intent.putExtra("botones",1);
            ProductActivity.this.startActivity(intent);
            // do something here and don't write super.onBackPressed()
        } else if (name == "Quiniela") {
            Intent myIntent = new Intent(ProductActivity.this, QuinielaFechasActivy.class);
//                        myIntent.putExtra("key", value); //Optional parameters
            ProductActivity.this.startActivity(myIntent);
            // do something here and don't write super.onBackPressed()
        }
    }


        private void updateUI() {

//                        int caller = getIntent().getIntExtra("button", 0);
//            if (caller == 0){
//                GlobalClass globalVariable = (GlobalClass) getApplicationContext();
//                globalVariable.setJornada("Jornada15");
//            }

            int num1;
            int num2;
            int resultado;
            int vacio = 0;
            int dobles = 0;
            int triples = 0;
            int valueQuiniela = 0;
            Boolean pleno15 = false;
            Boolean Errores = false;
            ProductLab productLab = ProductLab.get(this);
            GlobalClass globalVariable = (GlobalClass) getApplicationContext();
            String nombre = globalVariable.getName();
            List<Product> products = null;
            ReciboLab reciboLab = ReciboLab.get(this);

            List<ReciboModel> recibos = reciboLab.getRecibo();

            if (nombre == "MJ") {
                products = productLab.getProductsMJ(getMyData());
                for (int i = 0; i < products.size(); i++) {
//            if (products.get(i).getCategoria() == "Quiniela")
                    if (products.get(i).isChecked()) {

                        Product product = new Product();
                        ReciboModel recibo = new ReciboModel();
                        recibo.setTitle(products.get(i).getTitle());
                        recibo.setDescripcion(products.get(i).getDescripcion());
                        recibo.setPrecio(products.get(i).getPrecio());
//                product.setDescripcion(products.get(i).getDescripcion());
//                product.setPrecio(products.get(i).getPrecio());
                        if (products.get(i).getCantidad().length() == 0) {
                            num2 = Integer.valueOf("1");
                        } else {
                            num2 = Integer.valueOf(products.get(i).getCantidad());
                        }
                        recibo.setCantidad(products.get(i).getCantidad());// Every other one
                        num1 = Integer.valueOf(products.get(i).getPrecio());

                        resultado = num1 * num2;
                        String resString = String.valueOf(resultado);

                        recibo.setTotal(resString);
                        products.get(i).setCantidad("");

                        products.get(i).setChecked(false);

//                    finish();
//                    startActivity(getIntent());

                        recibos.add(recibo);



                    }
                }
                Intent myIntent = new Intent(ProductActivity.this, ReciboActivity.class);
//                        myIntent.putExtra("key", value); //Optional parameters
                ProductActivity.this.startActivity(myIntent);
            } else if (nombre == "Pizza Place") {
                products = productLab.getProducts(getMyData());
                for (int i = 0; i < products.size(); i++) {
//            if (products.get(i).getCategoria() == "Quiniela")
                    if (products.get(i).isChecked()) {

                        Product product = new Product();
                        ReciboModel recibo = new ReciboModel();
                        recibo.setTitle(products.get(i).getTitle());
                        recibo.setDescripcion(products.get(i).getDescripcion());
                        recibo.setPrecio(products.get(i).getPrecio());
//                product.setDescripcion(products.get(i).getDescripcion());
//                product.setPrecio(products.get(i).getPrecio());
                        if (products.get(i).getCantidad().length() == 0) {
                            num2 = Integer.valueOf("1");
                        } else {
                            num2 = Integer.valueOf(products.get(i).getCantidad());
                        }
                        recibo.setCantidad(products.get(i).getCantidad());// Every other one
                        num1 = Integer.valueOf(products.get(i).getPrecio());

                        resultado = num1 * num2;
                        String resString = String.valueOf(resultado);

                        recibo.setTotal(resString);
                        products.get(i).setCantidad("");

                        products.get(i).setChecked(false);

//                    finish();
//                    startActivity(getIntent());

                        recibos.add(recibo);
                    }
                }
                Intent myIntent = new Intent(ProductActivity.this, ReciboActivity.class);
//                        myIntent.putExtra("key", value); //Optional parameters
                ProductActivity.this.startActivity(myIntent);
            } else if (nombre == "Quiniela") {
                products = productLab.getProductsQuiniela(getMyData());

                for (int i = 0; i < products.size(); i++) {
                    if (!products.get(i).unoisChecked() && !products.get(i).dosisChecked() && !products.get(i).equisisChecked()) {
                        vacio = vacio + 1;
                        Errores = true;
                    }
                    if (i == 14) {

                        if (((products.get(i).unoisChecked() && products.get(i).dosisChecked() && !(products.get(i).equisisChecked()) ||
                                (products.get(i).equisisChecked() && products.get(i).dosisChecked() && !products.get(i).unoisChecked()) ||
                                (products.get(i).equisisChecked() && products.get(i).unoisChecked()) && !products.get(i).dosisChecked())) ||
                                (products.get(i).unoisChecked() && products.get(i).dosisChecked() && products.get(i).equisisChecked())) {
                            pleno15 = true;
                            Errores = true;
                        }
                    }
                    if ((products.get(i).unoisChecked() && products.get(i).dosisChecked() && !(products.get(i).equisisChecked()) ||
                            (products.get(i).equisisChecked() && products.get(i).dosisChecked() && !products.get(i).unoisChecked()) ||
                            (products.get(i).equisisChecked() && products.get(i).unoisChecked()) && !products.get(i).dosisChecked())) {
                        dobles = dobles + 1;


                    }

                    if (products.get(i).unoisChecked() && products.get(i).dosisChecked() && products.get(i).equisisChecked()) {
                        triples = triples + 1;

                    }

                    if ((dobles == 1 || dobles == 2) && triples > 8){
                        Errores = true;

                    }

                }





                        if(vacio > 0) {
                            String vacioStr = String.valueOf(vacio);
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                            alertDialog.setMessage("Falta " + vacioStr + " partidos por rellenar");
                            alertDialog.show();
                            Errores = true;
                        }
                        if(pleno15) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                            alertDialog.setMessage("No puede haber ni dobles ni triples en el pleno al 15");
                            alertDialog.show();
                            Errores = true;
                        }
                        if((dobles == 1 || dobles == 2) && triples > 8){
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                            alertDialog.setMessage("No puede haber  uno o dos  dobles y mas de ocho triples" );
                            alertDialog.show();
                            Errores = true;
                        }
                if(dobles == 3  && triples > 7){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber  tres  dobles y mas de 7 triples" );
                    alertDialog.show();
                }
                if((dobles == 4 || dobles == 5) && triples > 6){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber  cuatro o cinco  dobles y mas de 6 triples" );
                    alertDialog.show();
                    Errores = true;
                }
                if((dobles == 6 || dobles == 7) && triples > 5){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber  seis  o siete  dobles y mas de 5 triples" );
                    alertDialog.show();
                    Errores = true;
                }
                if(dobles == 8  && triples > 4) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber  ocho  dobles y mas de cuatro triples");
                    alertDialog.show();
                    Errores = true;
                }
                if((dobles == 9 || dobles == 10) && triples > 3){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber  nueve  o diez  dobles y mas de tres triples" );
                    alertDialog.show();
                    Errores = true;
                }
                if(dobles == 11  && triples > 2) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber  once  dobles y mas de dos triples");
                    alertDialog.show();
                    Errores = true;
                }
                if((dobles == 12 || dobles == 13) && triples > 1){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber doce o trece  dobles y mas de un triple" );
                    alertDialog.show();
                    Errores = true;
                }
                if(dobles == 14  && triples > 0){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber catorce dobles y algun triple" );
                    alertDialog.show();
                    Errores = true;
                }
                if( triples > 9){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
                    alertDialog.setMessage("No puede haber mas de 9 triples" );
                    alertDialog.show();
                    Errores = true;
                }
//            Toast.makeText(getApplicationContext(), vacioStr , Toast.LENGTH_SHORT).show();
                int jornada = getMyData();
                if (!Errores) {
                    for (int i = 0; i < products.size(); i++) {
                        Product product = new Product();
                        ReciboModel recibo = new ReciboModel();
                        String tripleStr = String.valueOf(i);


                        if(dobles == 1 && i == 14){
                            if(triples == 0) {
                                valueQuiniela = 3000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 1){
                                valueQuiniela = 12500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 2){
                                valueQuiniela = 38000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 3){
                                valueQuiniela = 113500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 4){
                                valueQuiniela = 340000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 5){
                                valueQuiniela = 1020000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 6){
                                valueQuiniela = 3061000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 7){
                                valueQuiniela = 9185000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 8){
                                valueQuiniela = 27500000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }
                        }else if (dobles == 2 && i == 14 ) {
                            if(triples == 0) {
                                valueQuiniela = 8500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 1){
                                valueQuiniela = 25000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 2){
                                valueQuiniela = 75500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 3){
                                valueQuiniela = 227000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 4){
                                valueQuiniela = 680000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 5){
                                valueQuiniela = 2041000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 6){
                                valueQuiniela = 6123000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 7){
                                valueQuiniela = 18370000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 8){
                                valueQuiniela = 27500000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }
                        }else if (dobles == 3 && i == 14) {
                            if(triples == 0) {
                                valueQuiniela = 17000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 1){
                                valueQuiniela = 50500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 2){
                                valueQuiniela = 151500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 3){
                                valueQuiniela = 453500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 4){
                                valueQuiniela = 1360000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 5){
                                valueQuiniela = 4082000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 6){
                                valueQuiniela = 12247000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 7){
                                valueQuiniela = 36741000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }
                        }else if (dobles == 4 && i == 14) {
                            if(triples == 0) {
                                valueQuiniela = 33500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 1){
                                valueQuiniela = 101000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 2){
                                valueQuiniela = 302500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 3){
                                valueQuiniela = 907000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 4){
                                valueQuiniela = 2721000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 5){
                                valueQuiniela = 8164000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 6){
                                valueQuiniela = 24494000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }
                        }else if (dobles == 5 && i == 14) {
                            if(triples == 0) {
                                valueQuiniela = 67000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 1){
                                valueQuiniela = 201500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 2){
                                valueQuiniela = 605000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 3){
                                valueQuiniela = 1814000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 4){
                                valueQuiniela = 5443000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 5){
                                valueQuiniela = 16329000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 6){
                                valueQuiniela = 48988000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }
                        }else if (dobles == 6 && i == 14) {
                            if(triples == 0) {
                                valueQuiniela = 134500;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 1){
                                valueQuiniela = 403000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 2){
                                valueQuiniela = 1210000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 3){
                                valueQuiniela = 3629000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 4){
                                valueQuiniela = 10886000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }else if (triples == 5){
                                valueQuiniela = 32659000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }
                        }else if (dobles == 7 && i == 14) {
                                if(triples == 0) {
                                    valueQuiniela = 269000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 1){
                                    valueQuiniela = 806500;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 2){
                                    valueQuiniela = 2420000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 3){
                                    valueQuiniela = 7257500;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 4){
                                    valueQuiniela = 21772000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 5){
                                    valueQuiniela = 65318000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }
                            }else if (dobles == 8 && i == 14) {
                                    if(triples == 0) {
                                        valueQuiniela = 537500;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }else if (triples == 1){
                                        valueQuiniela = 1612500;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }else if (triples == 2){
                                        valueQuiniela = 4838500;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }else if (triples == 3){
                                        valueQuiniela = 14515000;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }else if (triples == 4){
                                        valueQuiniela = 43545000;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }
                        }else if (dobles == 9 && i == 14) {
                                if(triples == 0) {
                                    valueQuiniela = 1075000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 1){
                                    valueQuiniela = 3225500;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 2){
                                    valueQuiniela = 9676500;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 3){
                                    valueQuiniela = 29030000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }
                        }else if (dobles == 10 && i == 14) {
                                if(triples == 0) {
                                    valueQuiniela = 2150000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 1){
                                    valueQuiniela = 6451000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 2){
                                    valueQuiniela = 19353500;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }else if (triples == 3){
                                    valueQuiniela = 58060000;
                                    String quinielaString = String.valueOf(valueQuiniela);
                                    recibo.setTotal(quinielaString);
                                    Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                }
                        }else if (dobles == 11 && i == 14) {
                                    if(triples == 0) {
                                        valueQuiniela = 4300000;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }else if (triples == 1){
                                        valueQuiniela = 12900000;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }else if (triples == 2){
                                        valueQuiniela = 38707000;
                                        String quinielaString = String.valueOf(valueQuiniela);
                                        recibo.setTotal(quinielaString);
                                        Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                    }
                        } else if (dobles == 12 && i == 14) {
                                        if(triples == 0) {
                                            valueQuiniela = 8600000;
                                            String quinielaString = String.valueOf(valueQuiniela);
                                            recibo.setTotal(quinielaString);
                                            Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                        }else if (triples == 1){
                                            valueQuiniela = 25805000;
                                            String quinielaString = String.valueOf(valueQuiniela);
                                            recibo.setTotal(quinielaString);
                                            Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                                        }
                            } else if (dobles == 14 && i == 14) {
                            if(triples == 0) {
                                valueQuiniela = 34406000;
                                String quinielaString = String.valueOf(valueQuiniela);
                                recibo.setTotal(quinielaString);
                                Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                            }
                        }

                        else if (triples == 1 && i == 14 && dobles == 0) {
                        valueQuiniela = 4500;
                        String quinielaString = String.valueOf(valueQuiniela);
                        recibo.setTotal(quinielaString);
                        Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 2 && i == 14 && dobles == 0) {
                            valueQuiniela = 19000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 3 && i == 14 && dobles == 0) {
                            valueQuiniela = 57000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 4 && i == 14 && dobles == 0) {
                            valueQuiniela = 170000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 5 && i == 14 && dobles == 0) {
                            valueQuiniela = 510000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 6 && i == 14 && dobles == 0) {
                            valueQuiniela = 1530000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 7 && i == 14 && dobles == 0) {
                            valueQuiniela = 4592000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 8 && i == 14 && dobles == 0) {
                            valueQuiniela = 13778000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 9 && i == 14 && dobles == 0) {
                            valueQuiniela = 41334000;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else if (triples == 0 && i == 14 && dobles == 0) {
                        valueQuiniela = 1500;
                        String quinielaString = String.valueOf(valueQuiniela);
                        recibo.setTotal(quinielaString);
                        Toast.makeText(getApplicationContext(), quinielaString , Toast.LENGTH_SHORT).show();
                        }else {
                            valueQuiniela = 0;
                            String quinielaString = String.valueOf(valueQuiniela);
                            recibo.setTotal(quinielaString);
                            Toast.makeText(getApplicationContext(), quinielaString, Toast.LENGTH_SHORT).show();
                        }

                        recibo.setPartido(products.get(i).getPartido());
                        if (jornada == 0 && i == 0){
                            recibo.setCategoria("Jornada 15");
                        }else if (jornada == 1 && i == 0){
                            recibo.setCategoria("Jornada 16");
                        }else {
                            recibo.setCategoria("quiniela");
                        }
                        if(products.get(i).unoisChecked()){
                            recibo.setmResultadoUno("1");
                        }
                        if(products.get(i).equisisChecked()){
                            recibo.setmResultadoEquis("X");
                        }
                        if(products.get(i).dosisChecked()){
                            recibo.setmResultadoDos("2");
                        }
                        recibos.add(recibo);



                    }

                    Intent myIntent = new Intent(ProductActivity.this, ReciboActivity.class);
//                        myIntent.putExtra("key", value); //Optional parameters
                    ProductActivity.this.startActivity(myIntent);

//                    Intent myIntent = new Intent(ProductActivity.this, ReciboActivity.class);
////                        myIntent.putExtra("key", value); //Optional parameters
//                    ProductActivity.this.startActivity(myIntent);

//                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
//                        alertDialog.setMessage("No hay errores" );
//                        alertDialog.show();


                    }
//                for (int i = 0; i < products.size(); i++) {
//                    if (!products.get(i).unoisChecked() && !products.get(i).dosisChecked() && !products.get(i).equisisChecked()) {
//                        vacio = vacio + 1;
//
//                    }
//
//                    if ((products.get(i).unoisChecked() && products.get(i).dosisChecked() && !(products.get(i).equisisChecked()) ||
//                            (products.get(i).equisisChecked() && products.get(i).dosisChecked() && !products.get(i).unoisChecked()) ||
//                            (products.get(i).equisisChecked() && products.get(i).unoisChecked()) && !products.get(i).dosisChecked())) {
//                        dobles = dobles + 1;
//
//                    }
//
//                    if (products.get(i).unoisChecked() && products.get(i).dosisChecked() && products.get(i).equisisChecked()) {
//                        triples = triples + 1;
//
//                    }
//
//                    if (i == 14) {
//
//                        if (((products.get(i).unoisChecked() && products.get(i).dosisChecked() && !(products.get(i).equisisChecked()) ||
//                                (products.get(i).equisisChecked() && products.get(i).dosisChecked() && !products.get(i).unoisChecked()) ||
//                                (products.get(i).equisisChecked() && products.get(i).unoisChecked()) && !products.get(i).dosisChecked())) ||
//                                (products.get(i).unoisChecked() && products.get(i).dosisChecked() && products.get(i).equisisChecked())) {
//                            pleno15 = true;
//                        }
//                    }
//                }

            }

//
//                else if (name == "Quiniela") {
//
//                    Product product = new Product();
//                    ReciboModel recibo = new ReciboModel();
//                    if (!products.get(i).unoisChecked() && !products.get(i).dosisChecked() && !products.get(i).equisisChecked()) {
//                        vacio = vacio + 1;
//
//                    }
//
//                    if ((products.get(i).unoisChecked() && products.get(i).dosisChecked() && !(products.get(i).equisisChecked()) ||
//                            (products.get(i).equisisChecked() && products.get(i).dosisChecked() && !products.get(i).unoisChecked()) ||
//                            (products.get(i).equisisChecked() && products.get(i).unoisChecked()) && !products.get(i).dosisChecked())) {
//                        dobles = dobles + 1;
//
//                    }
//
//                    if (products.get(i).unoisChecked() && products.get(i).dosisChecked() && products.get(i).equisisChecked()) {
//                        triples = triples + 1;
//
//                    }
//
//                    if (i == 14) {
//
//                        if (((products.get(i).unoisChecked() && products.get(i).dosisChecked() && !(products.get(i).equisisChecked()) ||
//                                (products.get(i).equisisChecked() && products.get(i).dosisChecked() && !products.get(i).unoisChecked()) ||
//                                (products.get(i).equisisChecked() && products.get(i).unoisChecked()) && !products.get(i).dosisChecked())) ||
//                                (products.get(i).unoisChecked() && products.get(i).dosisChecked() && products.get(i).equisisChecked())) {
//                            pleno15 = true;
//                        }
//                    }
//
//
////                products.get(i).setunoChecked(false);
////                products.get(i).setdosChecked(false);
////                products.get(i).setequisChecked(false);
//
////                finish();
////                startActivity(getIntent());
////
////                recibos.add(recibo);
//
//                }
//            }


//            else {
//                String dobleStr = String.valueOf(dobles);
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
//                alertDialog.setMessage( dobleStr);
//                alertDialog.show();
////            Toast.makeText(getApplicationContext(), vacioStr , Toast.LENGTH_SHORT).show();
//
//            }

//            else {
//                String tripleStr = String.valueOf(triples);
//                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
//                alertDialog.setMessage( tripleStr);
//                alertDialog.show();
////            Toast.makeText(getApplicationContext(), vacioStr , Toast.LENGTH_SHORT).show();
//
//            }
//            else {
//                if (pleno15) {
//                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProductActivity.this);
//                    alertDialog.setMessage("No se permite ni dobles ni triples en el pleno al quince");
//                    alertDialog.show();
//                }
//            Toast.makeText(getApplicationContext(), vacioStr , Toast.LENGTH_SHORT).show();

//            }

// else {
//
//                finish();
//                startActivity(getIntent());
//
//            }
//            }
//                        int caller = getIntent().getIntExtra("button", 0);
//            if (getMyData() == 0){
//                GlobalClass globalVariable1 = (GlobalClass) getApplicationContext();
//                globalVariable1.setJornada("Jornada15");
//                String jornada = globalVariable1.getJornada();
//                Toast.makeText(getApplicationContext(), jornada , Toast.LENGTH_SHORT).show();
//            }

        }


        }


