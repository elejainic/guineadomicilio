package ecuaapp.com.todoadomicilio;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by jaime.esono on 25/09/2016.
 */
public class TipoComidaAdapter extends RecyclerView.Adapter<TipoComidaHolder> {

    Context c;
    String[] enunciado;
    int[] imgs;
    int local;

    public TipoComidaAdapter(Context ctx, String[] names, int[] images, int lugar){
        this.c=ctx;
        this.enunciado=names;
        this.imgs=images;
        this.local = lugar;
    }



    @Override
    public TipoComidaHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.tipo_comida_model,null);

        TipoComidaHolder holder=new TipoComidaHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(TipoComidaHolder holder, final int position) {

        holder.img.setImageResource(imgs[position]);
        holder.nameTxt.setText(enunciado[position]);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
//
//                PizzaPlaceActivity activity = (PizzaPlaceActivity) (v);
//                Integer myDataFromActivity = PizzaPlaceActivity.getMyData();
//                String posicion;
//                Intent intent;
//                switch (pos) {
//                    case 0:
//                         posicion  = "puding";


                     Intent   intent = new Intent(c, ProductActivity.class);


                        intent.putExtra("button",pos);
//                        intent.putExtra("boton",local);

                        c.startActivity(intent);
//                    case 1:
//                         posicion  = "carnes";
//                        intent = new Intent(c, ProductActivity.class);
//
//                        intent.putExtra("carnes",pos);
//                        c.startActivity(intent);
//
//                        break;
//                }

            }
        });

    }

    @Override
    public int getItemCount() {

        return enunciado.length;
    }
}

