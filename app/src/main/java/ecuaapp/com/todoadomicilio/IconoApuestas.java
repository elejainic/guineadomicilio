package ecuaapp.com.todoadomicilio;

/**
 * Created by jaime.esono on 29/11/2016.
 */
public class IconoApuestas {

    public IconoApuestas( int img) {

        this.img=img;
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    private String name;
    private int img;


}

