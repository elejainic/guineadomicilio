package ecuaapp.com.todoadomicilio;

/**
 * Created by jaime.esono on 28/11/2016.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jaime.esono on 13/09/2016.
 */
public class AdapterLoteria extends BaseAdapter{

    Context c;
    ArrayList<IconoApuestas> iconos;


    public AdapterLoteria(Context c,ArrayList<IconoApuestas> iconos){

        this.c=c;
        this.iconos=iconos;

    }
    @Override
    public int getCount() {
        return iconos.size();
    }

    @Override
    public Object getItem(int pos) {
        return iconos.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {

        LayoutInflater inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.iconoapuestas, parent, false);
        }
        ImageView img = (ImageView) convertView.findViewById(R.id.imageView2);
//        TextView tv =(TextView) convertView.findViewById(R.id.textView2);

        img.setImageResource(iconos.get(pos).getImg());
//        tv.setText(iconos.get(pos).getName());

        return convertView;
    }
}
