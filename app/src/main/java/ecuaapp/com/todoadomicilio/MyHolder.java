package ecuaapp.com.todoadomicilio;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jaime.esono on 20/09/2016.
 */
public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView img;
    TextView nameTxt;
    TextView posTxt;
    ItemClickListener itemClickListener;

    public MyHolder(View itemView){
        super(itemView);

        img= (ImageView) itemView.findViewById(R.id.thumbnail);
        nameTxt = (TextView) itemView.findViewById(R.id.title);
        posTxt = (TextView) itemView.findViewById(R.id.description);

        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        this.itemClickListener.onItemClick(v,getLayoutPosition());

    }

    public void setItemClickListener(ItemClickListener ic)
    {
       this.itemClickListener=ic;
    }
}
