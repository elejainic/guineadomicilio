package ecuaapp.com.todoadomicilio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jaime.esono on 30/09/2016.
 */
public class ReciboListFragment extends android.support.v4.app.Fragment{

    private static final String TAG = "ProductListFragment";
    private static final int REQUEST_CRIME = 1;

    private RecyclerView mProductRecyclerView;
    private ReciboAdapter mAdapter;
    private List<ReciboModel> mRecibo;

    private boolean mItemhasChanged = false;
    private UUID mItemChangedId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recibo_list_fragment, container, false);

        mProductRecyclerView = (RecyclerView) view
                .findViewById(R.id.recibo_recycler_view);
        mProductRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());
        mProductRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }
    private void updateUI() {

        ReciboLab reciboLab = ReciboLab.get(getActivity());
        List<ReciboModel> recibos = reciboLab.getRecibo();
        mRecibo = new ArrayList<>();
//        for (int i = 0; i < recibos.size(); i++) {
//            if (recibos.get(i).isChecked()) {
//                Product product = new Product();
//                product.setTitle(products.get(i).getTitle());
//                product.setDescripcion(products.get(i).getDescripcion());
//                product.setPrecio(products.get(i).getPrecio());
//                product.setCantidad(products.get(i).getCantidad());// Every other one
//                mProduct.add(product);
//            }
//        }

        if (mAdapter == null) {
            mAdapter = new ReciboAdapter(recibos);
            mProductRecyclerView.setAdapter(mAdapter);
        } else {
            if (mItemhasChanged) {
                int mItemChangedPosition = mAdapter.getCrimeIndex(mItemChangedId);
                Log.d(TAG, "Changed position :" + mItemChangedPosition);
                mAdapter.notifyItemChanged(mItemChangedPosition);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: ");
        if (resultCode != Activity.RESULT_OK) {
            mItemhasChanged = false;
            Log.d(TAG, "not OK: " + resultCode);
            return;
        }

//        if (requestCode == REQUEST_CRIME) {
//            if (data != null) {
//                Log.d(TAG, "data != null");
//                mItemhasChanged = CrimeActivity.hasCrimeChanged(data);
//                mItemChangedId = CrimeActivity.getCrimeId(data);
//            }
//        }
    }

    private class ReciboHolder extends RecyclerView.ViewHolder  {

        private ReciboModel mRecibo;

//        private CheckBox mCheckedBox;
        private TextView mTitle;
        private TextView mDescripcion;
        private TextView mPrecio;
        private TextView mCantidad;
        private TextView mTotal;
        private TextView mPrueba;
        private TextView mPartido;
        private TextView mPrecioRecibo;
        private TextView mXcfRecibo;
        private TextView mCantidadRecibo;
        private TextView mTotalCantidad;
        private TextView mResultado1;
        private TextView mResultadoX;
        private TextView mResultado2;


        public ReciboHolder(View itemView) {
            super(itemView);

            mTitle = (TextView) itemView.findViewById(R.id.recibo_title);
            mDescripcion = (TextView) itemView.findViewById(R.id.recibo_descripcion);
            mPrecio = (TextView) itemView.findViewById(R.id.txtPrecio_recibo);
            mCantidad = (TextView) itemView.findViewById(R.id.txt_Cantidad_recibo);
            mTotal = (TextView) itemView.findViewById(R.id.total);
            mPrueba = (TextView) itemView.findViewById(R.id.prueba);
            mPartido = (TextView) itemView.findViewById(R.id.txtpartido);

            mPrecioRecibo = (TextView) itemView.findViewById(R.id.precio_recibo);
            mXcfRecibo = (TextView) itemView.findViewById(R.id.xcf_recibo);
            mCantidadRecibo = (TextView) itemView.findViewById(R.id.cantidad_recibo);
            mTotalCantidad = (TextView) itemView.findViewById(R.id.total_recibo);
            mResultado1 = (TextView) itemView.findViewById(R.id.txt_resultado_1);
            mResultadoX = (TextView) itemView.findViewById(R.id.txt_resultado_X);
            mResultado2 = (TextView) itemView.findViewById(R.id.txt_resultado_2);




//            mCheckedBox = (CheckBox)
//                    itemView.findViewById(R.id.product_selected);
      }

        public void bindRecibo(ReciboModel recibo) {
            mRecibo = recibo;
            mTitle.setText(mRecibo.getTitle());
            mDescripcion.setText(mRecibo.getDescripcion());
//            mCheckedBox.setChecked(mProduct.isChecked());
            mPrecio.setText(mRecibo.getPrecio());
            mCantidad.setText(mRecibo.getCantidad());
            mTotal.setText(mRecibo.getTotal());
            mPartido.setText(mRecibo.getPartido());
            mResultado1.setText(mRecibo.getResultadoUno());
            mResultadoX.setText(mRecibo.getResultadoEquis());
            mResultado2.setText(mRecibo.getmResultadoDos());
            GlobalClass globalVariable = (GlobalClass) getActivity().getApplicationContext();
            String name = globalVariable.getName();
           if (mRecibo.getCategoria() == "quiniela" || mRecibo.getCategoria() == "Jornada 15" ||
                   mRecibo.getCategoria() == "Jornada 16") {
               mTitle.setVisibility(View.GONE);
                mPrueba.setVisibility(View.GONE);
               mDescripcion.setVisibility(View.GONE);
               mCantidad.setVisibility(View.GONE);
               mTotal.setVisibility(View.GONE);
               mPrecioRecibo.setVisibility(View.GONE);
               mXcfRecibo.setVisibility(View.GONE);
               mCantidadRecibo.setVisibility(View.GONE);
               mTotalCantidad.setVisibility(View.GONE);




           }
        }


    }

    private class ReciboAdapter extends RecyclerView.Adapter<ReciboHolder> {

        private List<ReciboModel> mRecibo;

        public ReciboAdapter(List<ReciboModel> recibos) {
            mRecibo = recibos;
        }

        @Override
        public ReciboHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.carrito_model_fragment, null);

            ReciboHolder holder=new ReciboHolder(v);

            return holder;
        }

        @Override
        public void onBindViewHolder(ReciboHolder holder, int position) {
            ReciboModel recibo = mRecibo.get(position);
            holder.bindRecibo(recibo);

            holder.mTitle.setText(recibo.getTitle());
            holder.setIsRecyclable(false);

        }

        @Override
        public int getItemCount() {
            return mRecibo.size();
        }

        private int getCrimeIndex(UUID productId) {
            for (int i = 0; i < mRecibo.size(); i++) {
                ReciboModel recibo = mRecibo.get(i);
                if (recibo.getId().equals(productId)) {
                    return i;
                }
            }
            return -1;
        }
    }
}
