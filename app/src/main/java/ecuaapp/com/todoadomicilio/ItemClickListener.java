package ecuaapp.com.todoadomicilio;

import android.view.View;

/**
 * Created by jaime.esono on 20/09/2016.
 */
public interface ItemClickListener {

    void onItemClick(View v, int pos);
}
