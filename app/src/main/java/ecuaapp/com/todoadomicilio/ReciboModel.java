package ecuaapp.com.todoadomicilio;

import java.util.UUID;

/**
 * Created by jaime.esono on 03/10/2016.
 */
public class ReciboModel {
    private UUID mId;
    private String mTitle;
    private String mDescripcion;
    private boolean mChecked;
    private String mPrecio;
    private String mCantidad;
    private String mTotal;
    private String mPartido;
    private String mResultadoUno;
    private String mResultadoEquis;
    private String mResultadoDos;
    private String mCategoria;



    public ReciboModel() {
        // Generate unique identifier
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getCategoria() {
        return mCategoria;
    }

    public void setCategoria(String categoria) {
        mCategoria = categoria;
    }

    public String getDescripcion() {
        return mDescripcion;
    }

    public void setDescripcion(String descripcion) {
        mDescripcion = descripcion;
    }

    public String getPrecio() {
        return mPrecio;
    }

    public void setPrecio(String precio) {
        mPrecio = precio;
    }

    public String getCantidad() {
        return mCantidad;
    }

    public void setCantidad(String cantidad) {
        mCantidad = cantidad;
    }

    public String getPartido() {
        return mPartido;
    }

    public void setPartido(String partido) {
        mPartido = partido;
    }

    public String getResultadoEquis() {
        return mResultadoEquis;
    }

    public void setmResultadoEquis(String resultado) {
        mResultadoEquis = resultado;
    }

    public String getResultadoUno() {
        return mResultadoUno;
    }

    public void setmResultadoUno(String resultado) {
        mResultadoUno = resultado;
    }

    public String getmResultadoDos() {
        return mResultadoDos;
    }

    public void setmResultadoDos(String resultado) {
        mResultadoDos = resultado;
    }

    public String getTotal() {
        return mTotal;
    }

    public void setTotal(String total) {
        mTotal = total;
    }
//    public String toString(){
//        return mTitle || " " || mDescripcion || " " || mPrecio;
//    }

}

